(ns doomlist.infra.util
  #?(:cljs (:require-macros doomlist.infra.util))
  (:require [clojure.string]
            [clojure.set :refer [map-invert]]
            [net.cgrand.xforms]
            [medley.core]
            [taoensso.truss]
            [mount.core]
            [doomlist.infra.potemkix :refer [import-vars import-all]]
            #?(:cljs [tailrecursion.priority-map :as prio]
               :clj [clojure.data.priority-map :as prio])
            #?(:clj [doomlist.infra.mount])
            #?(:clj [doomlist.infra.log])))

#?(:clj (import-all doomlist.infra.log))
#?(:clj (import-all doomlist.infra.mount))

#?(:clj (import-vars
         [taoensso.truss have have! have? with-dynamic-assertion-data]))

(import-vars
 [clojure.string ends-with? starts-with? join includes? split replace-first lower-case upper-case]
 [prio priority-map priority-map-by priority-map-keyfn priority-map-keyfn-by]
 [medley.core find-first map-keys map-kv map-vals map-entry filter-kv filter-vals remove-vals remove-kv remove-keys least greatest deref-swap! deref-reset!])

(defn join-some
  ([coll] (join-some "" coll))
  ([sep coll] (join sep (remove empty? coll))))

(def replace-all clojure.string/replace)

(defmacro def- [n & forms]
  `(def ~(vary-meta n assoc :private true) ~@forms))

(defmacro doto>> [x & forms]
  (let [gx (gensym)]
    `(let [~gx ~x]
       ~@(map (fn [f]
                (with-meta
                  (if (seq? f)
                    `(~@f ~gx)
                    `(~f ~gx))
                  (meta f)))
              forms)
       ~gx)))

#?(:clj (if (System/getenv "DISPLAY")
          (do (require 'inspector-jay.core)
              (def inspect (ns-resolve 'inspector-jay.core 'inspect)))
          (defn inspect [x]
            (spy x))))

(defmacro xfor [& args]
  `(net.cgrand.xforms/for ~@args))

(defmacro forv [[x coll & binds] body]
  `(into [] (xfor [~x ~'% ~@binds] ~body) ~coll))

(defmacro for-map
  ([[x coll & binds] body]
   `(into {} (xfor [~x ~'% ~@binds] ~body) ~coll))
  ([head body-key body-val]
   `(for-map ~head (map-entry ~body-key ~body-val))))

(defmacro for-into
  ([to [x coll & binds] body]
   `(into ~to (xfor [~x ~'% ~@binds] ~body) ~coll))
  ([to head body-key body-val]
   `(for-into ~to ~head (map-entry ~body-key ~body-val))))

(defn bimap [m]
  (into m (map-invert m)))

(defn nonzero? [x]
  (not (zero? x)))

(defmacro for-indexed
  ([[x coll & binds] body]
   `(for [~x (map-indexed vector ~coll) ~@binds] ~body)))

(defn =by [f]
  (fn [& args] (apply = (map f args))))

#?(:cljs (defn- check-nan [x]
           (if (js/isNaN x)
             (throw (ex-info "got #NaN" {:val x}))
             x)))

(defn parse-int [x]
  #?(:cljs (check-nan (some-> x js/parseInt))
     :clj (if x (Long/parseLong x))))

(defn split-address [addr]
  {:pre [(string? addr)]}
  (update (doto (split addr #":" 2)
            (-> count (= 2) (assert (str "missing port in address" addr))))
          1 parse-int))

(defn join-address [addr port]
  {:pre [addr port]}
  (str addr \: port))

(defmacro while-let [[form tst] & body]
  `(loop []
     (when-let [~form ~tst]
       ~@body
       (recur))))

(defmacro while-some [[form tst] & body]
  `(loop []
     (when-some [~form ~tst]
       ~@body
       (recur))))

(defn now-ms []
  #?(:clj (System/currentTimeMillis)
     :cljs (.getTime (js/Date.))))

(defn round [x]
  #?(:clj (Math/round (double x))
     :cljs (Math/round x)))

(defn flip [f] #(apply f %2 %1 %&))

(defmacro defstate* [sym & args]
  `(mount.core/defstate
     ~(vary-meta sym (flip into) {:on-lazy-start :throw
                                  :on-reload :noop
                                  :private true})
     ~@args))

(defmacro defstate [sym start-form]
  `(defstate* ~sym
     :start (let [c# ~start-form]
              (have map? c# :data (str ~(name sym) " start returned non-map")))
     :stop ((:stop (deref ~sym) #(debug ~(name sym) "no-op stop")))))

#?(:clj
   (defn name-thread [name]
     (.setName (Thread/currentThread) name)))

(defn dissoc-in [m [k & ks]]
  (if ks
    (update m k dissoc-in ks)
    (dissoc m k)))

(defn trim [s len]
  (if s (subs s 0 (min len (count s)))))

(defn first-key [x]
  (some-> x first key))

(defn rseq* [x]
  (some-> x rseq))

(defn rlast [x]
  (first (rseq* x)))

(defn last-key [x]
  (some-> x rlast key))

(def server-order (juxt (comp (fnil - 0) :numhumans) :numspec :addr :port))
(def server-map (partial priority-map-keyfn server-order))
