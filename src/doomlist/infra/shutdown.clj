(ns doomlist.infra.shutdown
  {:clojure.tools.namespace.repl/unload false}
  (:require [doomlist.infra.util]
            [mount.core]))

(defonce hook (Thread. (fn []
                         (doomlist.infra.util/warn "shutdown")
                         (mount.core/stop)
                         (doomlist.infra.util/info "shutdown done"))))
