(ns doomlist.infra.modinfo
  (:require [doomlist.infra.util :refer :all]))

; https://wiki.zandronum.com/List_of_mods

(def- modes {:cooperative "Cooperative"
             :survival "Survival"
             :invasion "Invasion"
             :deathmatch "Deathmatch"
             :teamdm "Team deathmatch"
             :duel "Duel"
             :terminator "Terminator"
             :lastmanstanding "Last man standing"
             :teamlms "Team LMS"
             :possession "Possesion"
             :teampossession "Team possession"
             :teamgame "Team game"
             :ctf "Capture the flag"
             :oneflagctf "Capture the flag"
             :skulltag "Skulltag"
             :domination "Domination"})

(def- by-mode? (System/getProperty "by-mode"))

(defn identify [{:keys [iwad pwads game gamename gametype] :as server}]
  (if by-mode?
    (modes (:gamemode gametype))
    (let [pwads (mapv lower-case pwads)
          lwad (lower-case iwad)]
      (cond
        (starts-with? lwad "megagame") "MegaMan"
        (starts-with? lwad "chex") "Chex Quest"
        (starts-with? lwad "wolf3d") "Wolfenstein 3D"
        (and (not (#{"doom2.wad" "doom.wad" "tnt.wad" "freedoom.wad"
                     "freedoom2.wad" "plutonia.wad"} lwad))
             (not (starts-with? gamename "DOOM"))) (trim (or gamename iwad) 25)
        (= :ctf (:gamemode gametype)) "Capture the flag"
        (some #(re-seq #"^jm_core-|^jumpmaze" %) pwads) "Jumpmaze"
        (some #(re-seq #"^aow[-_.]?v?2" %) pwads) "All Out War 2"
        (some #(re-seq #"^aow[-_.]" %) pwads) "All Out War"
        (some #(re-seq #"^doomz-" %) pwads) "DoomZ"
        (some #(re-seq #"^floppydiskmod" %) pwads) "Floppy Disk Mod"
        (some #(re-seq #"^bfh" %) pwads) "Bosses From Hell"
        (some #(re-seq #"^zeberpain" %) pwads) "Zeberpain"
        (some #(re-seq #"^samsara" %) pwads) "Samsara"
        (some #(re-seq #"^prophunt" %) pwads) "Prop Hunt"
        (some #(re-seq #"^qcde[-_.v]" %) pwads) "Quake Champions: Doom Edition"
        (some #(re-seq #"^hvm[-_.v]" %) pwads) "Hell vs. Marines"
        (some #(re-seq #"^lod[-_.v]" %) pwads) "Ghouls vs. Humans"
        (some #(re-seq #"^bd64[g0-9]" %) pwads) "Brutal Doom 64"
        (some #(re-seq #"^bwolf" %) pwads) "Brutal Wolfenstein"
        (some #(re-seq #"^project[- ]brutality" %) pwads) "Project Brutality"
        (some #(re-seq #"^brutalv[0-9]|^bd[0-9][0-9]rc" %) pwads) "Brutal Doom"
        (some #(re-seq #"^ns_core" %) pwads) "Ghouls vs. Humans"
        (some #(re-seq #"^wolfx" %) pwads) "Wolfenstein X"
        (some #(re-seq #"^sf(renzy|plus)[-_.v0-9]" %) pwads) "Shotgun Frenzy"
        (some #(re-seq #"^rga2" %) pwads) "Realguns Hardcore"
        (some #(re-seq #"^stwolf" %) pwads) "Wolfenstein 3D"
        (some #(re-seq #"^beastincarn" %) pwads) "Beast Incarnation"
        (some #(re-seq #"^realgunsadv" %) pwads) "Realguns Advanced"
        (some #(re-seq #"^hellshots_golf[-_.v]" %) pwads) "Hellshots Golf"
        (some #(re-seq #"^dnd[-_.v]" %) pwads) "Death and Decay"
        (some #(re-seq #"^dbzone[-_.v]" %) pwads) "Doom Barracks Zone"
        (some #(re-seq #"^byoc[-_.v]" %) pwads) "Bring Your Own Class"
        (some #(re-seq #"^d4t[-_.v]" %) pwads) "Death Foretold"
        (some #(re-seq #"inhumanmons" %) pwads) "Inhuman Monstrosity"
        (some #(re-seq #"^hns[-_.v]" %) pwads) "Hide 'n' Seek"
        (some #(re-seq #"^gvh" %) pwads) "Ghouls vs. Humans"
        (some #(re-seq #"^idioticlms[-_.v]" %) pwads) "Idiotic LMS"
        (some #(re-seq #"^(classic_)?deathrun[-_.v0-9]" %) pwads) "Deathrun"
        (some #(re-seq #"^superdemon[-_.v0-9]" %) pwads) "SuperDemon"
        (some #(re-seq #"^gmota[-_.v]" %) pwads) "GMOTA"
        (some #(re-seq #"^fdm" %) pwads) "Floppy Disk Mod"
        (some #(re-seq #"^fragfest" %) pwads) "FragFest"
        (some #(re-seq #"^fartyguns" %) pwads) "Fartyguns"
        (some #(re-seq #"^zblood" %) pwads) "ZBlood"
        (some #(re-seq #"^dball[-_.v]" %) pwads) "Dodgeball"
        (some #(re-seq #"^vietdoom" %) pwads) "VietDoom"
        (some #(re-seq #"^deathball" %) pwads) "Deathball"
        (some #(re-seq #"^wos[-_.]" %) pwads) "Whispers of Satan"
        (some #(re-seq #"^doomcenter[-_.]" %) pwads) "Doom Center"
        (some #(re-seq #"^ghouls_world" %) pwads) "Ghoul's World"
        (some #(re-seq #"^strnghld[-_.]" %) pwads) "Stronghold"
        (some #(re-seq #"^wrathofcronos|^woc_" %) pwads) "Wrath of Cronos"
        (some #(re-seq #"^mutilator[-_.]" %) pwads) "Who Dun It + Mutilator"
        (some #(re-seq #"^(wdi|whodunit)[-_.]" %) pwads) "Who Dun It"

        (some #(re-seq #"^iop[-_.]" %) pwads)
        (if (some #(re-seq #"^stupidlms" %) pwads)
          "Icon of Party + Stupid LMS"
          "Icon of Party")

        (some #(re-seq #"^stupidlms[-_.]" %) pwads) "Stupid LMS"
        (some #(re-seq #"^zh-b[0-9]+[-_.]" %) pwads) "Bagel Horde"
        (some #(re-seq #"^mof?p[0-9-_.]" %) pwads) "Master of Puppets"
        (some #(re-seq #"^hard-doom" %) pwads) "Hard Doom"
        (some #(re-seq #"^zdoomwars" %) pwads) "ZDoom Wars"
        (some #(re-seq #"^goldeneye" %) pwads) "Golden Eye"
        (some #(re-seq #"^ge007" %) pwads) "Golden Eye"
        (some #(re-seq #"^bossbattles" %) pwads) "Boss Battles"
        (some #(re-seq #"^(sf?)?d64" %) pwads) "Doom 64"
        (some #(re-seq #"^doom64" %) pwads) "Doom 64"
        (some #(re-seq #"^zh[-_.]" %) pwads) "Zombie Horde"
        (some #(re-seq #"^rj[-_.]" %) pwads) "Rocket Jump"
        (some #(re-seq #"^zt2[-_.]" %) pwads) "Zombie Torrent 2"
        (some #(re-seq #"^doomware[-_.]" %) pwads) "DoomWare"
        (some #(re-seq #"^supercharge[-_.]" %) pwads) "Supercharge"
        (some #(re-seq #"^sectorcraft" %) pwads) "SectorCraft"
        (some #(re-seq #"^madcatmorph" %) pwads) "Madcat Morphs"
        (some #(re-seq #"^complex-doom-invasion[-_.]" %) pwads) "Complex Invasion"
        (some #(re-seq #"^complex-cf[-_.]|clusterfuck" %) pwads) "Clusterfuck"
        (some #(re-seq #"^lsd[-_.]" %) pwads) "LSD"
        (some #(re-seq #"^lca-(djb|ark)[-_.]" %) pwads) "Complex/LCA/+"
        (some #(re-seq #"^lca[-_.]" %) pwads) "Complex/LCA"
        (some #(re-seq #"^complex[-_.]" %) pwads) "Complex Doom"
        (some #(re-seq #"^ro[-_.]" %) pwads) "Russian Overkill"
        (some #(re-seq #"^invasion" %) pwads) "Invasion"
        (some #(re-seq #"^idm[-_.]" %) pwads) "Insanity Deathmatch"
        (some #(re-seq #"aooni[-_.v]" %) pwads) "Ao Oni"
        (some #(re-seq #"^push[-_.]" %) pwads) "Push"

        (seq (get-in server [:extended-info :gamemode-name]))
        (get-in server [:extended-info :gamemode-name])

        (or (starts-with? lwad "doom")
            (starts-with? lwad "plutonia")
            (starts-with? lwad "tnt")) (modes (:gamemode gametype))
        :default (some-> gamename (trim 10)
                         (replace-first #"DOOM" "Doom")
                         (replace-all #" " "\u00a0"))))))
