(ns doomlist.infra.log
  (:require [potemkin]
            [clojure.string :as s]
            [clojure.stacktrace :refer [print-cause-trace]]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.community.rolling
             :refer [rolling-appender]]))

(potemkin/import-vars
 [timbre error warn info debug trace])

(defn- stacktrace [{:keys [?err]}] (with-out-str (print-cause-trace ?err)))

(defn- wrap [x] (s/replace-first x #"] - " "]\n"))

(def ts-pattern "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")

(timbre/merge-config!
 {:timestamp-opts {:timezone (java.util.TimeZone/getDefault)
                   :pattern ts-pattern}
  :output-fn #(wrap (timbre/default-output-fn {:error-fn stacktrace} %))
  :appenders {:println {:min-level (or (keyword
                                         (System/getProperty "print-level"))
                                       :info)}
              :rolling (-> {:path (or (System/getProperty "log-path")
                                      "log/doomlist.log")}
                           rolling-appender
                           (assoc :min-level :debug
                                  :async? true))}
  :min-level [["io.netty.*" :warn]
              ["taoensso.sente" :warn]
              ["doomlist.refresher.server" :debug]
              ["doomlist.protocol.zandronum" :info]
              ["io.methvin.watcher.*" :warn]
              ["org.eclipse.jetty.*" :warn]
              ["*" :debug]]})

(defmacro spy
  ([expr] `(timbre/spy ~expr))
  ([name expr] `(timbre/spy :debug ~name ~expr))
  ([level name expr] `(timbre/spy ~level ~name ~expr)))

(defmacro spy>
  ([expr] `(timbre/spy ~expr))
  ([expr name] `(timbre/spy :debug ~name ~expr))
  ([expr level name] `(timbre/spy ~level ~name ~expr)))

(defmacro logged-future [& args]
  `(timbre/logged-future
    (try
      ~@args
      (catch InterruptedException ~'_))))
