(ns doomlist.infra.main
  (:gen-class)
  (:require [doomlist.infra.util :refer :all]
            [doomlist.infra.shutdown :as shutdown]
            [doomlist.refresher.server]
            [doomlist.ui.page]
            [doomlist.ui.api]
            [doomlist.server.http]
            [doomlist.server.websocket]
            [doomlist.protocol.zandronum :as p]
            [mount.core]))

(def- report
  (delay
   (let [p (java.lang.ProcessHandle/current)]
     (some-> (.. p info command (orElse nil)) debug)
     (debug "$" (.. p info commandLine (orElse ""))
            (seq (.. p info arguments (orElse ""))))
     (debug "PID =" (.pid p)))
   (debug "*assert* =" *assert*)
   (debug "launch order:" (->> @@#'mount.core/meta-state
                               (sort-by (comp :order val))
                               (mapv key)
                               (join ", ")))))

(defn start []
  (Thread/setDefaultUncaughtExceptionHandler
   (reify Thread$UncaughtExceptionHandler
     (uncaughtException [_ thread ex]
       (error ex "Uncaught exception on thread" (.getName thread)))))
  (swap! @#'mount.core/meta-state assoc-in
         [(str #'doomlist.server.http/server) :order] Long/MAX_VALUE)
  @report
  (try
    (.addShutdownHook (Runtime/getRuntime) shutdown/hook)
    (catch IllegalArgumentException _ (comment "hook already set")))
  (mount.core/start))

(defn -main []
  (start))
