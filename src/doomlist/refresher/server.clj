(ns doomlist.refresher.server
  (:require [clojure.set :as s]
            [doomlist.infra.geoloc :as geo]
            [doomlist.infra.util :refer :all]
            [doomlist.refresher.master :refer :all]
            [doomlist.protocol.udp :as udp]
            [doomlist.infra.modinfo :as modinfo]
            [doomlist.protocol.textcolors :as t]
            [doomlist.protocol.zandronum :as p]))

(defn- updated-map-history [history ready ts mapname]
  (let [lst (:mapname (last history))]
    (if (= lst mapname)
      history
      (cond-> (if history
                (conj history {:mapname mapname :ts ts})
                (conj clojure.lang.PersistentQueue/EMPTY
                      (cond-> {:mapname mapname :ts ts}
                        (not (realized? ready)) (assoc :incomplete true))))
        (< 8 (count history)) pop))))

(defn- location [addr country]
  (assoc (if-let [c (geo/countries country)]
           {:code country
            :name c}
           (geo/locate addr))
         :sv_country country))

(defn- gamemode [{:keys [gamemode-name gamemode-shortname]}]
  (cond-> nil
    gamemode-name (assoc :name gamemode-name)
    gamemode-shortname (assoc :shortname gamemode-shortname)))

(defn- postproc [{:keys [url playerdata addr extended-info mapname] :as info}
                 reqtime restime]
  {:post [(:addr %) (:port %)]}
  ;(inspect info)
  (-> info
      (dissoc :extended-info)
      (assoc :ping (- restime reqtime)
             :ts restime
             :url (if (not= "\"\"" url) url)
             :reqtime reqtime
             :mapname (some-> mapname upper-case)
             :location (location addr (:country extended-info))
             :gamemode (gamemode extended-info)
             :voicechat (:voicechat extended-info)
             :mod (modinfo/identify info)
             :numhumans (count (remove :bot playerdata))
             :numplaying (count (remove (some-fn :spec :bot) playerdata))
             :numspec (count (filter (every-pred :spec (complement :bot))
                                     playerdata))
             :numbots (count (filter :bot playerdata))
             :playerdata (mapv (fn [{:keys [name] :as player}]
                                 (assoc player :plain-name (t/plain-name name)))
                               playerdata))))

(defn- update-server [state server reqtime ts info]
  (let [s (get-in state [:servers server])
        post (postproc info reqtime ts)
        map-history (updated-map-history
                     (:map-history s) (:ready state) ts (:mapname post))
        final (merge post
                     {:map-history map-history}
                     (if-not (:pwads info)
                       ; short query => keep previous full query info
                       (select-keys s (conj p/long-flags :mod)))
                     ; https://zandronum.com/tracker/view.php?id=4148
                     ; this compensates for servers that report zero players
                     ; for a short period of time during game mode changes:
                     (if (and (:map-history s)
                              (zero? (:numplayers info))
                              (pos? (:numhumans s))
                              (not= map-history (:map-history s)))
                       (select-keys s [:playerdata :numhumans :numplaying
                                       :numspec :numbots])))]
    (-> state
        (assoc-in [:servers server] final)
        (assoc :ts ts :last server :expired nil))))

; {:current {:size :count} :segments [...] :reqtime :done}
(defn- update-stage
  [{:keys [done] {cursize :size cnt :count} :current :as staged}
   {:keys [number total readsize size data]}]
  {:pre [(< total 100) (< readsize 10000)]}
  (if (= total 1)
    (assoc staged :done [size [data]] :segments nil :current nil)
    (let [segments (if (and (= size cursize) (= cnt total)
                            (not (some? (nth (:segments staged) number))))
                     (assoc (or (:segments staged)
                                (vec (make-array Object total)))
                            number data)
                     (-> (vec (make-array Object total))
                         (assoc number data)))]
      (if (every? some? segments)
        (assoc staged :done [size segments] :segments nil :current nil)
        (assoc staged :done nil :segments segments
               :current {:size size :count total})))))

(defn- pr-segments [segments]
  (str \[ (join (map #(if (some? %) \X \_) segments)) \]))

(defn- refresher-receiver [state stage socket]
  (while (and (not (Thread/interrupted)) (not (.isClosed socket)))
    (try
      (let [{:keys [addr port] :as raw} (udp/receive socket)
            server (join-address addr port)]
        (try
          (let [data (p/parse-server-response raw)
                reqtime (get-in @stage [server :reqtime])]
            (if (:number data) ; segmented
              (let [s (-> (swap! stage update server update-stage data)
                          (get server))
                    [size segments :as res] (:done s)]
                (if res
                  (do (trace "updating server info - segments:"
                             (count segments) server)
                      (swap! state update-server server reqtime (now-ms)
                             (assoc (p/parse-segments size segments)
                                    :addr addr :port port)))
                  (trace "got segment" (:number data)
                         (pr-segments (:segments s)) server)))
              (do (trace "updating server info" server)
                  (swap! state update-server server reqtime (now-ms)
                         (assoc data :addr addr :port port)))))
          (catch InterruptedException e (throw e))
          (catch Throwable e
            (if (some-> (ex-data e) :code)
              (info (ex-message e) server)
              (warn e "failed to update server info" server)))))
      (catch InterruptedException e (throw e))
      (catch Throwable e
        (warn e "receiver socket error")))))

(def expiry-ms
  (or (some-> (System/getProperty "expiry-sec") parse-int (* 1000))
      (* 1000 60 3)))

(def refresh-interval-ms
  (or (some-> (System/getProperty "server-refresh-interval-sec")
              (-> parse-int (max 5) (* 1000)))
      (* 45 1000)))

(def inactive-refresh-interval-ms
  (or (some-> (System/getProperty "inactive-server-refresh-interval-sec")
              (-> parse-int (max 5) (* 1000)))
      (* 3 refresh-interval-ms)))

(def timeout-expiry-ms
  (or (some-> (System/getProperty "timeout-expiry-sec") parse-int (* 1000))
      (int (* 2.5 inactive-refresh-interval-ms))))

(def persisted-list-expiry-ms
  (or (some-> (System/getProperty "persisted-list-expiry-sec")
              parse-int
              (* 1000))
      (* 3 inactive-refresh-interval-ms)))

(assert (< refresh-interval-ms timeout-expiry-ms))
(assert (< master-refresh-interval-ms expiry-ms))
(assert (< refresh-interval-ms inactive-refresh-interval-ms))
(assert (< inactive-refresh-interval-ms persisted-list-expiry-ms))

(defn- cleanup-expired [state servers]
  (if-let [exp (->> servers
                    (keep (fn [[id {:keys [expired ts]}]]
                            (if (or (and expired
                                         (< expiry-ms (- (now-ms) expired)))
                                    (and ts
                                         (< timeout-expiry-ms
                                            (- (now-ms) (:start state)))
                                         (< timeout-expiry-ms (- (now-ms) ts))))
                              id)))
                    seq)]
    (assoc state :expired exp :last nil :ts (now-ms)
           :servers (apply dissoc servers exp))
    (assoc state :servers servers :last nil :expired nil)))

(def refresh-pause-ms
  (or (some-> (System/getProperty "server-refresh-pause-ms")
              (-> parse-int (min 100)))
      1))

(defn- refresher-sender [state stage socket ready list-ready]
  (debug "waiting for first master list")
  @ready
  (let [last-inactives (volatile! 0)
        last-full (volatile! 0)]
    (while (and (not (Thread/interrupted)) (not (.isClosed socket)))
      (let [inactives? (< inactive-refresh-interval-ms
                          (- (now-ms) @last-inactives))
            full? (and inactives? (< (* 1.5 inactive-refresh-interval-ms)
                                     (- (now-ms) @last-full)))
            servers (filter-vals (fn [{:keys [numhumans]}]
                                   (or inactives?
                                       (if numhumans (pos? numhumans))))
                                 (:servers @state))]
        (info "updating" (count servers) "servers"
              (if full? "(full)" "(short)"))
        (when inactives? (vreset! last-inactives (now-ms)))
        (when full? (vreset! last-full (now-ms)))
        (doseq [[id {:keys [addr port expired]}] (some-> (seq servers) shuffle)
                :while (not (.isClosed socket))
                :when (not expired)]
          (trace "querying server" id)
          (swap! stage update id assoc :reqtime (now-ms) :current nil)
          (try
            (if full?
              (udp/send socket addr port p/server-query)
              (udp/send socket addr port p/short-server-query))
            (catch InterruptedException e (throw e))
            (catch Throwable e
              (warn e "failed to query server" id)))
          (Thread/sleep refresh-pause-ms)))
      (Thread/sleep refresh-interval-ms)
      (deliver list-ready true))))

(defn- update-list [state {:keys [ts servers]}]
  (try
    (cleanup-expired
     state
     (reduce (fn [r [addr port]]
               (-> r
                   (update (join-address addr port) assoc
                           :master-updated ts
                           :addr addr :port port)
                   (dissoc :expired)))
             (reduce (fn [r [addr port]]
                       (update-in r [(join-address addr port) :expired]
                                  #(or % ts)))
                     (:servers state)
                     (s/difference (into #{} (map (comp (juxt :addr :port) val)
                                                  (:servers state)))
                                   servers))
             servers))
    (catch Throwable e
      (error e "unrecoverable: failed to process server list update")
      (System/exit 1))))

(def persist-file (System/getProperty "persist-file-path"))

(defn- persist-serverlist [state]
  (when persist-file
    (try
      (spit persist-file (binding [*print-length* nil
                                   *print-level* nil]
                           (pr-str (select-keys state [:servers :ts]))))
      (catch Throwable e
        (error e "failed to persist server list")))))

(defn- load-server [{:keys [map-history] :as v}]
  (cond-> v
    map-history
    (update :map-history #(into clojure.lang.PersistentQueue/EMPTY %))))

(defn- load-serverlist []
  (when persist-file
    (try
      (let [{:keys [ts servers]} (read-string (slurp persist-file))]
        (if (< (- (now-ms) persisted-list-expiry-ms) ts)
          {:ts ts :servers (reduce-kv (fn [r k v]
                                        (assoc r k (load-server v)))
                                      (server-map)
                                      servers)}
          (info "not using persisted serverlist - too old:" ts)))
      (catch java.io.FileNotFoundException e
        (info "not using persisted serverlist - file not found:" persist-file))
      (catch Throwable e
        (warn e "failed to load persisted server list")))))

(defn- update-list! [state master-servers]
  (some->> (swap! state update-list master-servers)
           :expired seq
           (debug "expired servers:")))

(defstate server-refresher
  (let [list-ready (promise) ; first refresh of all servers done
        state (atom {:servers (server-map) :ready list-ready :start (now-ms)}
                    :validator map?)
        stage (atom {})
        snapshots (atom (sorted-map) :validator sorted?)
        ready (promise) ; first master refresh done
        socket (udp/socket 0)
        sender (logged-future
                 (name-thread "server refresher sender")
                 (refresher-sender state stage socket ready list-ready))
        receiver (logged-future
                   (name-thread "server refresher receiver")
                   (refresher-receiver state stage socket))]
    (when-let [restored (load-serverlist)]
      (info "using persisted serverlist")
      (swap! state conj restored)
      (deliver list-ready true))
    (watch-masterlist ::k (fn [_ cur]
                            (update-list! state cur)
                            (deliver ready true)))
    (when (seq (:servers (current-masterlist)))
      (update-list! state (current-masterlist))
      (deliver ready true))
    {:state state
     :stage stage
     :snapshots snapshots
     :ready list-ready
     :stop (fn []
             (deliver ready false)
             (deliver list-ready false)
             (future-cancel sender)
             (future-cancel receiver)
             (.close socket)
             (persist-serverlist @state))}))

(defn await-list []
  (assert @(:ready @server-refresher)))

(defn watch-serverlist [k f]
  (add-watch (:state @server-refresher) k
             (fn [_ _ old cur]
               (assert (not (and (:last cur) (:expired cur))))
               (if (or (if (:last cur)
                         (not (identical? (:last cur) (:last old))))
                       (if (:expired cur)
                         (not (identical? (:expired cur) (:expired old)))))
                 (f old cur)))))

(defn current-serverlist []
  (let [{:keys [servers ts]} @(:state @server-refresher)]
    {:servers (filter-vals :ts servers)
     :ts ts}))

(defn- snapshot [s {:keys [ts servers] :as state}]
  (if (and ts (or (empty? s) (<= (+ (last-key s) 10000) ts)))
    (cond-> (assoc s ts servers)
      (< 15 (count s)) (dissoc (first-key s)))
    s))

(defn serverlist-snapshot
  ([snap]
   {:pre [(have? number? snap)]}
   (let [{:keys [snapshots state]} @server-refresher
         snaps (swap! snapshots #(if (seq %) % (snapshot % @state)))]
     (if-let [s (snaps snap)]
       {:servers s
        :ts snap}
       (reduce (fn find-last [res [ts s]]
                 (if (< snap ts)
                   (reduced res)
                   {:servers s
                    :ts ts}))
               nil
               snaps))))
  ([] (await-list)
      (let [{:keys [snapshots state]} @server-refresher
            [ts s] (rlast (swap! snapshots snapshot @state))]
        {:servers s
         :ts ts})))

(comment
  (defn query-server
    ([addr] (apply query-server (split-address addr)))
    ([addr port] (with-open [socket (udp/socket)]
                   (query-server addr port socket)))
    ([addr port socket]
     (debug "querying server" addr port)
     (udp/send socket addr port p/server-query-singlepacket)
     (p/parse-server-response (udp/receive socket))))
  (inspect (query-server "localhost:10666"))
  (inspect (postproc (query-server "localhost:10666") 0 0)))

(comment
  (def stage (atom {}))
  (defn query-server-multipacket
    ([addr] (apply query-server-multipacket (split-address addr)))
    ([addr port] (with-open [socket (udp/socket)]
                   (query-server-multipacket addr port socket)))
    ([addr port socket]
     (debug "querying server" addr port)
     (udp/send socket addr port p/server-query)
     (dotimes [_ 10]
       (let [data (udp/receive socket)
             parsed (p/parse-server-response data)]
         (inspect parsed)
         (inspect (swap! stage update-stage parsed))))))
  (inspect (query-server-multipacket "localhost:10666"))
  (inspect (apply p/parse-segments (:done @stage)))
  (assert (= (query-server "127.0.0.1:10666")
             (apply p/parse-segments (:done @stage)))))

(comment
  (inspect (take 5 (:servers (current-serverlist))))
  (inspect (filter-vals (comp :country :extended-info) (:servers (current-serverlist))))
  (inspect (filterv (comp seq :playerdata val) (:servers (current-serverlist))))
  (inspect (take 5 (filterv #(< 1 (count (:map-history (val %)))) (:servers (current-serverlist)))))
  (inspect (update @(:state @server-refresher) :servers (flip take) 15))
  (inspect (filterv (comp nil? :ts val) (:servers (serverlist-snapshot))))
  (inspect (get-in @(:state @server-refresher) [:servers "localhost:10666"])))

(comment
  (def s (select-keys @(:state @server-refresher) [:servers :ts]))
  (persist-serverlist s)
  (def l (load-serverlist))
  (assert (= s l)))
