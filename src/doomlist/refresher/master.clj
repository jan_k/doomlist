(ns doomlist.refresher.master
  (:require [doomlist.infra.util :refer :all]
            [doomlist.protocol.udp :as udp]
            [doomlist.protocol.zandronum :as p]))

(def default-master-server-addr "master.zandronum.com:15300")

(defn query-master
  ([addr] (apply query-master (split-address addr)))
  ([addr port] (with-open [socket (udp/socket)]
                 (query-master addr port socket)))
  ([addr port socket]
   (debug "querying master server" addr port)
   (udp/send socket addr port p/master-query)
   (let [res (p/parse-master-responses (repeatedly #(udp/receive socket)))]
     (debug "got" (count res) "servers from master server")
     res)))

(def master-refresh-interval-ms
  (or (some-> (System/getProperty "master-refresh-interval-sec")
              (-> parse-int (max 10) (* 1000)))
      (* 120 1000)))

(defn- master-addr []
  (split-address (or (System/getProperty "master-server")
                     default-master-server-addr)))

(defn- make-refresher [state]
  (let [[addr port] (master-addr)]
    (with-open [socket (udp/socket)]
      (while (not (Thread/interrupted))
        (try
          (reset! state {:servers (query-master addr port socket)
                         :ts (now-ms)})
          (catch InterruptedException e (throw e))
          (catch Throwable e
            (warn e "failed to load servers")))
        (Thread/sleep master-refresh-interval-ms)))))

(defstate master-refresher
  (let [state (atom {:servers nil} :validator #(contains? % :servers))
        f (logged-future
            (name-thread "master refresher")
            (make-refresher state))]
    {:state state
     :stop #(future-cancel f)}))

(defn current-masterlist []
  @(:state @master-refresher))

(defn watch-masterlist [k f]
  (add-watch (:state @master-refresher) k (fn [_ _ old cur] (f old cur))))

(comment
  (inspect (current-masterlist)))

(comment
  (->> (query-master default-master-server-addr)
       (group-by first)
       (map-vals (partial mapv second))
       inspect))
