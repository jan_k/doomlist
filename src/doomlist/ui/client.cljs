(ns doomlist.ui.client
  (:require [taoensso.sente :as sente]
            [taoensso.encore :as enc]
            [clojure.core.async :as a]
            [doomlist.ui.util :as u]
            [doomlist.infra.util :as v]))

(defmulti handle-event (fn [tag data] tag))
(defmethod handle-event :default [& args]
  (u/warn "unhandled event:" args))

(defn- handle-heartbeat [{:keys [heartbeat reconnect chsk] :as client}]
  (reset! heartbeat (v/now-ms))
  (when-not (:open? @(:state client))
    (when (< 10000 (- (v/now-ms) @reconnect))
      (u/log "resetting closed chsk")
      (reset! reconnect (v/now-ms))
      (sente/chsk-reconnect! chsk))))

(defn backoff-fn [retryno]
  (enc/exp-backoff retryno {:min 3000 :max 15000}))

(defn- make-client []
  (let [heartbeat (atom (v/now-ms))
        reconnect (atom (v/now-ms))
        {:keys [ch-recv send-fn] :as chsk}
        (assoc (sente/make-channel-socket-client!
                "/ws" "#" {:type :ws
                           :backoff-ms-fn backoff-fn
                           :wrap-recv-evs? true
                           :recv-buf-or-n 20000})
               :heartbeat heartbeat :reconnect reconnect)]
    (assert chsk)
    (a/go
      (v/while-some [{:keys [?data id event] :as x} (a/<! ch-recv)]
        (case id
          :chsk/handshake nil
          :chsk/state (let [[o n] ?data]
                        (u/log "sente state:" n)
                        (cond
                          (and (not (:open? o)) (:open? n))
                          (reset! reconnect (v/now-ms))

                          (and (:open? o) (not (:open? n)))
                          (handle-event :client/disconnected nil)

                          ; clicking steam link in firefox does 1001 disconnect
                          ; even when the page stays open
                          (= 1001 (:code (:last-ws-close n)))
                          (do (u/log "disconnected 1001")
                              (a/go
                                (<! (a/timeout 2000))
                                (u/log "forcing reconnect")
                                (reset! reconnect (v/now-ms))
                                (sente/chsk-reconnect! (:chsk chsk))))))
          :chsk/recv (do (handle-heartbeat chsk)
                         (apply handle-event ?data))
          (u/log "sente chsk" event)))
      (u/log "sente client end"))
    (a/go-loop []
      (a/<! (a/timeout 40000))
      (let [lst @heartbeat]
        (when (and lst (< 160000 (- (v/now-ms) lst))
                   (< 160000 (- (v/now-ms) @reconnect)))
          (u/log "forcing reconnect")
          (reset! reconnect (v/now-ms))
          (sente/chsk-reconnect! (:chsk chsk)))
        (if lst (recur))))
    (assoc chsk :stop (fn []
                        (reset! heartbeat nil)
                        (sente/chsk-disconnect! (:chsk chsk))
                        (a/close! ch-recv)))))

(v/defstate client (make-client))

(defn send! [arg]
  ((:send-fn @client) arg))

(defmethod handle-event :chsk/ws-ping [])

(defmethod handle-event :server/ping []
  (send! [:chsk/ws-ping]))

(defn state []
  (:state @client))

(comment
  (sente/chsk-reconnect! (:chsk @client)))
