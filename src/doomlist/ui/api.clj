(ns doomlist.ui.api
  (:require [clojure.core.memoize :as memo]
            [clojure.data.json :as json]
            [doomlist.server.http :refer [http-handler]]
            [doomlist.refresher.server :as r]
            [doomlist.infra.util :refer :all]))

(def always-full? (System/getProperty "full"))

(defn render-json [full?]
  (debug "rendering json")
  (let [{:keys [servers ts]} (r/serverlist-snapshot)]
    (if full?
      (json/write-str servers)
      (json/write-str (filter-vals #(pos? (:numhumans % 0)) servers)))))

(def- default-json-cache-ms 2000)

(def- json-cache-ms
  (or (parse-int (System/getProperty "json.cache.ms"))
      default-json-cache-ms))

(def- cached-json (if (pos? json-cache-ms)
                    (memo/ttl render-json {} :ttl/threshold json-cache-ms)
                    render-json))

(defn- response [{:keys [uri]}]
  {:body (cached-json (or always-full?
                          (some-> uri (ends-with? "full"))))
   :headers {"Content-Type" "application/json"
             "Cache-Control" "no-store"
             "Access-Control-Allow-Origin" "*"
             "Access-Control-Max-Age" "86400"}
   :status 200})

(defmethod http-handler :api [req]
  (info "api hit" (:remote-addr req))
  (response req))
