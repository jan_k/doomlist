(ns doomlist.ui.page
  (:require [rum.core :as rum]
            [taoensso.tempura :as tempura]
            [clojure.core.memoize :as memo]
            [doomlist.server.websocket :as ws]
            [doomlist.server.http :refer [http-handler]]
            [doomlist.ui.components :as c]
            [doomlist.refresher.server :as r]
            [doomlist.infra.util :refer :all]))

(def dict (or (System/getProperty "dictionary") "default"))
(def always-full? (System/getProperty "full"))

(def dictionary
  {:default {:game "Zandronum"
             :title "doomlist.net"
             :link "https://zandronum.com/"
             :favicon "favicon.ico"}
   :q {:game "QZandronum"
       :title "q.doomlist.net"
       :link "https://qzandronum.com/"
       :favicon "favicon_q.ico"}})

(def opts {:dict dictionary})
(def tr (partial tempura/tr opts [(keyword dict) :default]))

(defn render-page [static? full?]
  (debug "rendering" (if static? "static" "live") "page")
  (let [{:keys [servers ts]} (r/serverlist-snapshot)
        api-link (str "https://" (tr [:title]) "/api")
        state (atom {:servers servers :snapshot ts :full* full? :full full?})]
    (str "<!DOCTYPE html>\n"
         (rum/render-html
          [:html {:lang "en"}
           [:head
            [:title (or (System/getProperty "website.title")
                        (tr [:title "doomlist.net"]))]
            [:meta {:name "description"
                    :content (str "Server browser for " (tr [:game])
                                  ", a live list of " (tr [:game]) " servers")}]
            [:link {:rel "icon" :href (str \/ (tr [:favicon "favicon.ico"]))}]
            [:link {:href (str "/css/style.css" \? ws/version)
                    :rel "stylesheet" :type "text/css"}]]
           [:body
            [:div#root
             [:div.row
              [:div.col1 [:h2 (tr [:game]) " server list"]]
              (if-not static? [:div.col2#status (c/+status)])]
             (if-not (or static? always-full?) [:div#filter (c/+filter state)])
             [:div#list {:data-snapshot ts} (c/+list state)]]
            [:p [:small [:a {:href "https://gitlab.com/jan_k/doomlist"}
                         "https://gitlab.com/jan_k/doomlist"]
                 " | JSON data endpoint: "
                 [:a {:href api-link} api-link]
                 (if-not always-full?
                   " (append /full to include empty servers)")
                 [:br]
                 "To play, get " (tr [:game]) " at "
                 [:a {:href (tr [:link])} (tr [:link])]]]
            (if-not static?
              [:script {:src (str "/js/compiled/client.js"
                                  \? ws/version)}])]]))))

(def- default-website-cache-ms 10000)

(def- website-cache-ms
  (or (parse-int (System/getProperty "website.cache.ms"))
      default-website-cache-ms))

(def- cached-page (if (pos? website-cache-ms)
                    (memo/ttl render-page {} :ttl/threshold website-cache-ms)
                    render-page))

(defn- response [{:keys [uri]} static?]
  {:body (cached-page static? (or always-full?
                                  (some-> uri (ends-with? "full"))))
   :headers {"Content-Type" "text/html" "Cache-Control" "no-store"}
   :status 200})

(defmethod http-handler :main [req]
  (info "hit" (:remote-addr req))
  (response req false))

(defmethod http-handler :static [req]
  (info "static hit" (:remote-addr req))
  (response req :static))
