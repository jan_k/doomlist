(ns doomlist.ui.components
  (:require #?@(:cljs [[goog.object :as g]])
            [rum.core :as rum]
            [clojure.core.async :refer [go]]
            [doomlist.ui.util :as u]
            [doomlist.ui.flags :refer [flag]]
            [doomlist.infra.util :as v]))

(rum/defc +status < rum/reactive
  ([] (+status (delay nil) (delay nil)))
  ([state sente-state]
   (let [{:keys [error started init]} (rum/react state)
         {:keys [open?]} (rum/react sente-state)
         blink? (or error (and started (not open?)))
         color (cond
                 error "red"
                 (and init open?) "green"
                 started "orange"
                 :else "gray")]
     [:div.right
      [:br]
      [:span.status {:class [(if blink? "blink") color]
                     :title (cond
                              error (str "Page probably broke :(\n"
                                         "Try refreshing.\n\n"
                                         error)
                              init "Receiving live updates"
                              open? "Waiting for up-to-date server list"
                              started "Trying to connect for updates"
                              :else "Server connection inactive\nEnable Javascript for live updates")}
       "⬤"]])))

(defn- playercount [{:keys [numplayers numspec maxclients maxplayers numplaying numbots]}]
  (str (if (zero? numplayers)
         "Empty server"
         (str (v/join-some
               " + "
               [(if (pos? numplaying) (str numplaying " player"
                                           (if (not= 1 numplaying) \s)))
                (if (pos? numspec) (str numspec " spec"))
                (if (pos? numbots) (str numbots " bot"
                                        (if (not= 1 numbots) \s)))])))
       " / max " (min maxclients maxplayers) " players\n"))

(defn- players-tooltip [{:keys [playerdata] :as s}]
  (str (playercount s)
       (v/join "\n" (for [{:keys [plain-name spec bot score ping]}
                          (sort-by (juxt :spec :bot) playerdata)]
                      (str plain-name
                           (if bot " (bot)")
                           (if spec " (spect)"))))))

(defn- server-tooltip
  [{:keys [url version email] title :name}]
  (str title (if (seq version) (str "\nVersion: " version))
       ;(if (and (seq email) (not= "\"\"" email)) (str "\nE-mail: " email))
       (if (seq url) (str "\nURL: " url))))

(def lock "\uD83D\uDD12")

(defn- lock-tooltip
  [{:keys [forcepassword forcejoinpassword]}]
  (if-not (or forcepassword forcejoinpassword)
    "Open server"
    (v/join "\n" (cond-> []
                   forcejoinpassword (conj "Password protected")
                   forcepassword (conj "Password protected ingame")))))

(defn- game-tooltip
  [{:keys [iwad pwads] {:keys [gamemode gametype]} :gametype :as server}]
  (str "Game mode: " (get-in server [:gamemode :name] (name gamemode))
       (if (seq gametype)
         (str " (" (v/join " " (for [t gametype] (name t))) ")"))
       "\nIWAD: " iwad
       (if (seq pwads)
         (str "\nPWADs:\n" (v/join "\n" (cons iwad pwads))))))

(def block \u2588)
(def halfblock \u2592)
(def partblock \u2591)
(def space \u200a)

(defn- bars [tag cnt bar]
  (v/for-indexed [[i p] (->> (repeat cnt (str bar \u200a))
                             (partition-all 4))]
    [tag {:key i} [:span.nb p]]))

(defn- playerbar [{:keys [numbots numhumans numspec]}]
  [:div.playerbar.dj
   [:span.bar-player (repeat (- numhumans numspec) (str block space))]
   [:span.bar-spec (repeat numspec (str halfblock space))]
   [:span.bar-bot (repeat numbots (str partblock space))]])

(rum/defc +filter < rum/reactive [state]
  [:div.filter
   [:input#full {:type :checkbox
                 :on-change (fn [e]
                              #?(:cljs
                                 (let [s (-> e
                                             (g/get "target")
                                             (g/get "checked"))]
                                   (swap! state assoc :full* s)
                                   (u/set-url! (if s "/full" "/"))
                                   (go (swap! state assoc :full s)))))
                 :checked (boolean (:full* (rum/react state)))}]
   [:label {:for "full"} "\u200aShow empty servers"]])

(defn- minutes-between [nxt ts]
  (v/round (/ (- nxt ts) (* 1000 60))))

(defn- display-minutes [mins incomplete? last?]
  (cond
    (< mins 2) (cond
                 incomplete? "time unknown"
                 last? "just started"
                 :else "briefly")
    (< 600 mins) (str "long time" (if last? ", ongoing"))
    (< 180 mins) (str (v/round (/ mins 60)) (if incomplete? \+) " hours"
                      (if last? ", ongoing"))
    :else (str mins (if incomplete? \+) " minutes" (if last? ", ongoing"))))

(defn- map-history [history cts]
  (if (seq history)
    (str "Most recent maps:\n"
         (v/join "\n"
                 (reverse
                   (for [[{:keys [ts mapname incomplete]} {nxt :ts}]
                         (map vector history (concat (next history) [nil]))
                         :when (or (not nxt) (< (minutes-between cts ts) 600))
                         :let [mins (minutes-between (or nxt cts) ts)]]
                     (str mapname " ("
                          (display-minutes mins incomplete (not nxt)) ")")))))))

(defn- voicechat-tooltip [{:keys [voicechat]}]
  (case voicechat
    :disabled "Voice chat disabled"
    :global "Open voice chat"
    :team-based "Team-based voice chat"
    :spectators-separate "Players and spectators chat separately"
    (str "sv_voicechat = " voicechat)))

(rum/defc +list < rum/reactive [state]
  (let [{:keys [servers full snapshot]} (rum/react state)]
    (assert (or (empty? servers) (sorted? servers)))
    [:table
     [:colgroup [:col] [:col.narrow]]
     [:thead
      [:tr
       [:th.twe {:title "Password protection"} lock]
       [:th.pl (v/join (concat (repeat 4 \u00A0) "Players" (repeat 4 \u00A0)))]
       [:th.twe {:title "Location"} "\uD83C\uDF10"]
       [:th {:colSpan 2} "Server"]
       [:th "Game"]
       [:th "Map"]
       [:th (str "Address\u00A0" space)
        [:a.lnk {:href "/howto_address.html" :title "Guide on how to make the links work"} "[?]"]]]]
     [:tbody
      (for [[id {:keys [mapname location numhumans] :as s}] servers
            :when (or full (some-> numhumans pos?))
            :when (and (:name s) (not (:expired s)))
            :let [addr (v/join-address (:addr s) (:port s))
                  voice (if (:voicechat s)
                          (not= :disabled (:voicechat s))
                          (v/starts-with? (:version s) "3.2-alpha-r240405-2229"))]]
        [:tr {:key id}
         [:td.center.twe {:title (lock-tooltip s)}
          (if ((some-fn :forcepassword :forcejoinpassword) s) lock)]
         [:td {:title (players-tooltip s)} (playerbar s)]
         [:td.center {:title (:name location)} (flag (:code location))]
         [:td.wrap (cond-> {:title (server-tooltip s)}
                     (not voice) (assoc :colSpan 2)) (:name s)]
         (if (v/starts-with? (:version s) "3.2-alpha-r240405-2229")
           [:td.center.voice.faded {:title "This version supports voice chat but doesn't report whether it is enabled or not"}
            "\uD83D\uDD0A"]
           (if voice
             [:td.center.voice {:title (voicechat-tooltip s)}
              "\uD83D\uDD0A"]))
         [:td {:title (game-tooltip s)} (:mod s)]
         [:td {:title (or (if (pos? numhumans)
                            (map-history (:map-history s) snapshot))
                          (if (< 8 (count mapname)) mapname))}
          (v/trim mapname 8)]
         [:td [:a {:href (str "zan://" addr)} addr]]])]]))
