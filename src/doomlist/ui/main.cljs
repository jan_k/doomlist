(ns doomlist.ui.main
  (:require [goog.object :as g]
            [clojure.core.async :as a]
            [rum.core :as rum]
            [mount.core :as mount]
            [doomlist.ui.util :as u]
            [doomlist.ui.client :as client]
            [doomlist.ui.components :as c]
            [doomlist.infra.util :as v]))

(defonce ^:private state
  (let [full? (v/ends-with? js/window.location.href "/full")]
    (atom {:queue []
           :full full?
           :full* full?
           :snapshot (-> (u/element-by-id! "list")
                         (.getAttribute "data-snapshot")
                         v/parse-int)}
          :validator map?)))

(g/set js/window "onerror" #(swap! state assoc :error %5))

(defn- update-server [{:keys [servers] :as state} [id {:keys [ts] :as s}]]
  (cond-> (update state :snapshot max ts)
    (< (:ts (servers id) 0) (:ts s)) (assoc-in [:servers id] s)))

(defn- expire-servers [state ids ts]
  {:pre [(coll? ids)]}
  (update state :servers (v/flip reduce)
          (fn [servers id]
            (cond-> servers
              (< (:ts (servers id) 0) ts) (dissoc id)))
          ids))

(defmulti handle-update (fn [state [tag data]] tag))

(defmethod handle-update :server/update [state [tag data]]
  (update-server state data))
(defmethod handle-update :server/expire [state [tag data]]
  (apply expire-servers state data))

(derive :server/update ::state-change)
(derive :server/expire ::state-change)
(defmethod client/handle-event ::state-change [tag data]
  (if (:init @state)
    (swap! state handle-update [tag data])
    (swap! state update :queue conj [tag data])))

(defn- process-queue [state ts]
  (reduce handle-update
          (assoc state :init ts :snapshot ts :queue [])
          (:queue state)))

(defmethod client/handle-event :server/live [_ ts]
  (u/log "updates queued:" (count (:queue @state)))
  (swap! state process-queue ts)
  (u/log "synced with server" ts))

(defmethod client/handle-event :server/version [_ ver]
  (if (not= ver (:version (v/deref-swap! state assoc :version ver) ver))
    (a/go (v/info "reloading for new version" ver)
          (a/<! (a/timeout 250))
          (.reload js/document.location true))
    (let [{:keys [servers snapshot]} @state]
      (assert (number? snapshot))
      (client/send! [(doto (if (seq servers) :client/resume :client/init)
                       (as-> x (v/info "requesting" x "since" snapshot)))
                     snapshot]))))

(defmethod client/handle-event :server/snapshot [_ {:keys [servers ts]}]
  (let [requested (:snapshot @state)]
    (swap! state assoc :servers (into (v/server-map) servers) :snapshot ts)
    (if (= ts requested)
      (rum/hydrate (c/+list state) (u/element-by-id! "list"))
      (do (v/warn "received different snapshot " ts "; requested %s" requested)
          (rum/mount (c/+list state) (u/element-by-id! "list"))))))

(defmethod client/handle-event :client/disconnected [_ ver]
  (u/log "disconnected")
  (swap! state assoc :init nil :queue []))

(mount/start)

(rum/hydrate (c/+status state (client/state)) (u/element-by-id! "status"))
(if-let [f (u/element-by-id "filter")]
  (rum/hydrate (c/+filter state) f)
  (swap! state assoc :full true))

(swap! state assoc :started true)
