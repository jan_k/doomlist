(ns doomlist.server.http
  (:require [org.httpkit.server :as s]
            [ring.middleware.defaults :as rd]
            [ring.middleware.anti-forgery :refer :all]
            [ring.util.response :as ring]
            [doomlist.infra.util :refer :all]))

(defn- dispatch [{:keys [server-name uri]}]
  (cond
    (some-> server-name  (starts-with? "static.")) :static
    (starts-with? uri "/api") :api
    (= "/ws" uri) :sente
    (= "/chsk" uri) :sente
    :else :main))

(defmulti http-handler dispatch)

(defmethod http-handler :default [r]
  {:body "This doesn't exist.\n"
   :headers {"Content-Type" "text/plain"}
   :status 404})

(defn- handler [req]
  (try
    ;(inspect req)
    (http-handler req)
    (catch Throwable e
      (error e "http handler error")
      {:status 500
       :headers {"Content-Type" "text/plain"}
       :body "Failed to process request"})))

(def- default-listen-port 8765)

(defn- listen-port []
  (or (parse-int (System/getProperty "listen-port"))
      default-listen-port))

(defstate* ^{:on-reload :restart} server
  :start (let [port (listen-port)]
           (info "HTTP server starting on" (str "http://localhost:" port))
           (-> handler
               (rd/wrap-defaults (-> rd/site-defaults
                                     (dissoc :session)
                                     (dissoc-in [:security :anti-forgery])))
               (s/run-server {:port port})))
  :stop (@server))
