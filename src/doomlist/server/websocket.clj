(ns doomlist.server.websocket
  (:require [doomlist.server.http :refer [http-handler]]
            [clojure.core.async :as a]
            [doomlist.refresher.server :as refresher]
            [taoensso.sente :as sente]
            [taoensso.sente.interfaces :refer [pack unpack]]
            [taoensso.sente.server-adapters.http-kit :refer [get-sch-adapter]]
            [doomlist.infra.util :refer :all]))

(defonce version #=(java.lang.System/currentTimeMillis)) ; clients will force page reload when reconnecting to a changed version (ie. updated jar on server)

(def- packer @#'sente/default-edn-packer)

(defn- prepack [x]
  (pack packer x))

(defmethod print-method clojure.lang.PersistentQueue [x w]
  (print-method (seq x) w))

(def- packer*
  (reify taoensso.sente.interfaces/IPacker
    (pack [o r]
      (if (keyword? r)
        (prepack r)
        (let [[t & x] r]
          (if (keyword? t)
            (if (= "server" (namespace t))
              (str \[ t \space (join \space x) \])
              (prepack r))
            (str \[ (join \space (map #(pack o %) r)) \])))))
    (unpack [_ x] (unpack packer x))))

(defn- init-client [send-fn uid]
  (send-fn uid [:server/version version]))

(defn- stripped-server [s]
  (-> s
      (dissoc :ping :ts :reqtime :master-updated :expired)
      (update :playerdata (flip mapv)
              #(dissoc % :score :ping :minutes-playing))))

(def- server= (=by stripped-server))

(defn- expired-since [old cur]
  (for [[id _] old
        :when (not (contains? cur id))]
    id))

(defn- changed-since [old cur]
  (for [[id o] cur
        :when (not (server= o (old id)))]
    id))

(defn- resume [send-fn uid snap]
  (let [cur (refresher/current-serverlist)]
    (when (not= (:ts cur) (:ts snap))
      (when-let [ids (seq (expired-since (:servers snap) (:servers cur)))]
        (send-fn uid [:server/expire (prepack [ids (:ts cur)])]))
      (doseq [id (changed-since (:servers snap) (:servers cur))
              :let [s ((:servers cur) id)]
              :when (and (:ts s) (not (:expired s)))]
        (send-fn uid [:server/update (prepack [id s])])))
    (send-fn uid [:server/live (:ts cur)] {:flush? true})))

(defn- update-client-latest [send-fn uid]
  (let [snap (refresher/current-serverlist)]
    (debug "update client" uid "to latest" (:ts snap))
    (send-fn uid [:server/snapshot (prepack snap)] {:flush? true})
    (send-fn uid [:server/live (:ts snap)] {:flush? true})))

(defn- update-client [send-fn uid ts]
  {:pre [(have? number? ts)]}
  (if-let [snap (refresher/serverlist-snapshot ts)]
    (do (debug "update client" uid "from earlier" ts)
        (send-fn uid [:server/snapshot (prepack snap)] {:flush? true})
        (resume send-fn uid snap))
    (update-client-latest send-fn uid)))

(defn- resume-client [send-fn uid ts]
  {:pre [(have? number? ts)]}
  (debug "resume client" uid "since" ts)
  (if-let [snap (refresher/serverlist-snapshot ts)]
    (resume send-fn uid snap)
    (update-client-latest send-fn uid)))

(defn- process-sente-event
  [chsk {:keys [event id ?data send-fn uid ring-req client-id]} clients]
  (case id
    :chsk/ws-ping (do (trace "sente ping" event)
                      (swap! clients assoc uid (now-ms)))
    :chsk/uidport-open (do (debug "init client" uid (:remote-addr ring-req))
                           (init-client send-fn uid)
                           (swap! clients assoc uid (now-ms))
                           (info (count (:any @(:connected-uids chsk)))
                                 "ws clients"))
    :chsk/uidport-close (do (swap! clients dissoc uid)
                            (info (count (:any @(:connected-uids chsk)))
                                  "ws clients"))
    :client/init (update-client send-fn uid ?data)
    :client/resume (resume-client send-fn uid ?data)
    (debug "unhandled sente event" event)))

(defn- proc-update [clients send-fn old {:keys [last expired ts servers]}]
  (if-let [msg (cond
                 expired [:server/expire (prepack [expired ts])]
                 (and last (not (server= ((:servers old) last) (servers last))))
                 [:server/update (prepack [last (servers last)])])]
    (doseq [c (keys clients)]
      (send-fn c msg))))

(defn- make-ws-server []
  (let [a (agent nil :error-handler (fn [_ e] (error e "sente agent error")))
        clients (atom {})
        exit (a/chan)
        {:keys [ch-recv connected-uids send-fn] :as chsk}
        (sente/make-channel-socket-server!
         (get-sch-adapter) {:user-id-fn #(str (:client-id %) \@ (now-ms))
                            :packer packer*
                            :csrf-token-fn nil})]
    (debug "waiting for updated serverlist")
    (send-off a (fn [_]
                  (refresher/await-list)
                  (debug "got updated serverlist")
                  (refresher/watch-serverlist
                   ::ws #(proc-update @clients send-fn %1 %2))))
    (a/go
      (debug "sente started")
      (while-some [x (a/<! ch-recv)]
        (send a (fn [_] (process-sente-event chsk x clients))))
      (a/close! exit)
      (debug "sente closed"))
    (a/go-loop []
      (a/<! (a/timeout 60000))
      (when (a/alt! exit false :default true)
        (doseq [uid (:any @connected-uids)]
          (send-fn uid [:server/ping])
          (if-let [ts (@clients uid)]
            (when (< 120000 (- (now-ms) ts))
              (debug "disconnecting" uid)
              (send-fn uid [:chsk/close]))
            (swap! clients update uid #(or % (now-ms)))))
        (recur)))
    (assoc chsk :stop #(a/close! ch-recv) :clients clients)))

(defstate sente
  (make-ws-server))

(defmethod http-handler :sente [req]
  (case (:request-method req)
    :get ((:ajax-get-or-ws-handshake-fn @sente) req)
    :post ((:ajax-post-fn @sente) req)))

(comment
  (:any @(:connected-uids @sente)))

(comment
  (do (mount.core/stop #'sente)
      (mount.core/stop #'doomlist.server.http/server)
      (mount.core/start #'doomlist.server.http/server)
      (mount.core/start #'sente)))
