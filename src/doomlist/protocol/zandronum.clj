(ns doomlist.protocol.zandronum
  (:import [huffman Huffman]
           [java.io ByteArrayOutputStream]
           [java.nio ByteBuffer ByteOrder])
  (:require [doomlist.infra.util :refer :all]))

; https://wiki.zandronum.com/Launcher_protocol

(defn- bytebuf [size]
  (doto (ByteBuffer/allocate size) (.order ByteOrder/LITTLE_ENDIAN)))

(defn- wrapbuf [from]
  (doto (ByteBuffer/wrap from) (.order ByteOrder/LITTLE_ENDIAN)))

(defn- get! [m v]
  (or (m v) (throw (ex-info "unrecognized constant" {:value v}))))

(def- MASTER_CHALLENGE 5660028)
(def- MASTER_VERSION 2)

(def master-query
  (Huffman/encode (.array (doto (bytebuf 6)
                            (.putInt MASTER_CHALLENGE)
                            (.putShort MASTER_VERSION)))))

(defn- get-ubyte [^ByteBuffer buf]
  (Byte/toUnsignedLong (.get buf)))

(defn- get-ushort [^ByteBuffer buf]
  (Short/toUnsignedLong (.getShort buf)))

(defn- get-uint [^ByteBuffer buf]
  (Integer/toUnsignedLong (.getInt buf)))

(defn- get-string [^ByteBuffer buf]
  (loop [res (ByteArrayOutputStream.)
         b (get-ubyte buf)]
    (if (zero? b)
      (.toString res "utf-8")
      (recur (doto res (.write (int b)))
             (get-ubyte buf)))))

(def- master-response*
  (bimap {:banned 3
          :throttled 4
          :wrong-version 5
          :accepted 6
          :server-list-part 7
          :server-block 8
          :end-list 2}))

(defn- mark-packet [state no]
  (update state :packets (fnil conj (sorted-set)) (have! (complement neg?) no)))

(defn- add-servers [servers ip ports]
  (for-into (or servers #{}) [port ports]
    [ip port]))

(defn- master-read [state {:keys [^bytes data] :as packet}]
  {:pre [(map? state) (bytes? data) (pos? (alength data))]}
  (let [buf (wrapbuf (Huffman/decode data))
        code (get-uint buf)]
    (when-let [e (case (get! master-response* code)
                   :accepted nil
                   :banned "Master server denied - IP is banned"
                   :throttled "Master server denied - throttled"
                   :wrong-version "Master server denied - wrong version")]
      (throw (ex-info e {:code code})))
    (let [packetno (get-ubyte buf)]
      (have! #{(master-response* :server-block)} (get-ubyte buf))
      (loop [state (mark-packet state packetno)
             port-count (get-ubyte buf)]
        (if (zero? port-count)
          (case (get! master-response* (get-ubyte buf))
            :server-list-part state
            :end-list (if (not= (dec (count (:packets state)))
                                (first (rseq (:packets state))))
                        (throw (ex-info "missed some packets" state))
                        (reduced (:servers state))))
          (let [ip (join "." (repeatedly 4 #(get-ubyte buf)))
                ports (repeatedly port-count #(get-ushort buf))]
            (recur (update state :servers add-servers ip ports)
                   (get-ubyte buf))))))))

(defn parse-master-responses [responses]
  {:pre [(seq? responses)]
   :post [(set? %)]}
  (reduce master-read {} responses))

(def- flags*
  (bimap {:name              0x00000001
          :url               0x00000002
          :email             0x00000004
          :mapname           0x00000008
          :maxclients        0x00000010
          :maxplayers        0x00000020
          :pwads             0x00000040
          :gametype          0x00000080
          :gamename          0x00000100
          :iwad              0x00000200
          :forcepassword     0x00000400
          :forcejoinpassword 0x00000800
          :gameskill         0x00001000
          :botskill          0x00002000
          :dmflags           0x00004000
          :limits            0x00010000
          :teamdamage        0x00020000
          :teamscores        0x00040000
          :numplayers        0x00080000
          :playerdata        0x00100000
          :teaminfo-number   0x00200000
          :teaminfo-name     0x00400000
          :teaminfo-color    0x00800000
          :teaminfo-score    0x01000000
          :testing-server    0x02000000
          :data-md5sum       0x04000000
          :all-dmflags       0x08000000
          :security-settings 0x10000000
          :optional-wads     0x20000000
          :deh               0x40000000
          :extended-info     0x80000000}))
(def- flags2*
  (bimap {:pwad-hashes        0x00000001
          :country            0x00000002
          :gamemode-name      0x00000004
          :gamemode-shortname 0x00000008
          :voicechat          0x00000010
          }))

(def- LAUNCHER_CHALLENGE 199)

(def- query-fields #{:name :url :email :mapname :maxclients :maxplayers :pwads :gametype :gamename :iwad :forcepassword :forcejoinpassword :numplayers :playerdata :extended-info})

(def- query-flags
  (reduce bit-or (map flags* query-fields)))

(def long-flags [:url :email :pwads])

(def- short-query-flags
  (reduce bit-or (map flags* (apply disj query-fields long-flags))))

(def- extended-query-fields #{:country :gamemode-name :gamemode-shortname :voicechat})

(def- extended-flags (reduce bit-or (map flags2* extended-query-fields)))

(def server-query-singlepacket
  (Huffman/encode (.array (doto (bytebuf 16)
                            (.putInt LAUNCHER_CHALLENGE)
                            (.putInt query-flags)
                            (.putInt 0)
                            (.putInt extended-flags)))))

(def server-query
  (Huffman/encode (.array (doto (bytebuf 20)
                            (.putInt LAUNCHER_CHALLENGE)
                            (.putInt query-flags)
                            (.putInt 0)
                            (.putInt extended-flags)
                            (.putInt 2) ; request segmented response
                            ))))

(def short-server-query
  (Huffman/encode (.array (doto (bytebuf 20)
                            (.putInt LAUNCHER_CHALLENGE)
                            (.putInt short-query-flags)
                            (.putInt 0)
                            (.putInt extended-flags)
                            (.putInt 2) ; request segmented response
                            ))))

(def- parsed-flags
  [:name :url :email :mapname :maxclients :maxplayers :pwads :gametype :gamename :iwad :forcepassword :forcejoinpassword :gameskill :botskill :dmflags :limits :teamdamage :teamscores :numplayers :playerdata :extended-info])

(def- parsed-extended-flags [:country :gamemode-name :gamemode-shortname :voicechat])

(defmulti ^:private read-flag
  (fn read-flag-dispatch [f buf state] {:pre [(keyword? f)]} f))

(defmethod read-flag :name [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :url [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :email [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :mapname [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :maxclients [_ ^ByteBuffer buf _]
  (get-ubyte buf))

(defmethod read-flag :maxplayers [_ ^ByteBuffer buf _]
  (get-ubyte buf))

(def- gamemodes*
  (bimap {:cooperative     0
          :survival        1
          :invasion        2
          :deathmatch      3
          :teamdm          4
          :duel            5
          :terminator      6
          :lastmanstanding 7
          :teamlms         8
          :possession      9
          :teampossession  10
          :teamgame        11
          :ctf             12
          :oneflagctf      13
          :skulltag        14
          :domination      15}))

(def- teammodes
  #{:teamlms :teampossession :teamgame :oneflagctf :skulltag :domination :ctf :teamdm})

(defmethod read-flag :gametype [_ ^ByteBuffer buf _]
  (zipmap [:gamemode :gametype]
          [(get! gamemodes* (get-ubyte buf))
           (cond-> #{}
             (pos? (get-ubyte buf)) (conj :instagib)
             (pos? (get-ubyte buf)) (conj :buckshot))]))

(defmethod read-flag :gamename [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :iwad [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :forcepassword [_ ^ByteBuffer buf _]
  (nonzero? (get-ubyte buf)))

(defmethod read-flag :forcejoinpassword [_ ^ByteBuffer buf _]
  (nonzero? (get-ubyte buf)))

(defmethod read-flag :gameskill [_ ^ByteBuffer buf _]
  (get-ubyte buf))

(defmethod read-flag :botskill [_ ^ByteBuffer buf _]
  (get-ubyte buf))

(defmethod read-flag :dmflags [_ ^ByteBuffer buf _]
  (zipmap [:dmflags :dmflags2 :compatflags]
          [(get-uint buf) (get-uint buf) (get-uint buf)]))

(defmethod read-flag :limits [_ ^ByteBuffer buf _]
  (zipmap [:fraglimit :timelimit :timeleft :duellimit :pointlimit :winlimit]
          (repeatedly 6 #(get-ushort buf))))

(defmethod read-flag :teamdamage [_ ^ByteBuffer buf _]
  (.getFloat buf))

(defmethod read-flag :teamscores [_ ^ByteBuffer buf _]
  (zipmap [:blue :red] (repeatedly 2 #(get-ushort buf))))

(defmethod read-flag :numplayers [_ ^ByteBuffer buf _]
  (get-ubyte buf))

(defmethod read-flag :playerdata [_ ^ByteBuffer buf state]
  (->> (fn read-player []
         (-> (zipmap [:name :score :ping :spec :bot]
                     [(get-string buf) (.getShort buf) (get-ushort buf)
                      (nonzero? (.get buf)) (nonzero? (.get buf))])
             (cond->
              (teammodes (get-in state [:gametype :gamemode]))
               (assoc :team (get-ubyte buf)))
             (assoc :minutes-playing (get-ubyte buf))))
       (repeatedly (:numplayers state))
       vec))

(defmethod read-flag :pwads [_ ^ByteBuffer buf _]
  (into [] (remove empty?) (repeatedly (get-ubyte buf) #(get-string buf))))

(defmethod read-flag :country [_ ^ByteBuffer buf s]
  (let [res (reduce (fn [r v]
                      (let [c (char v)]
                        (if (Character/isLetter c)
                          (conj r (upper-case c))
                          (do (warn "invalid country character" v
                                    "from server" (:addr s) (:port s))
                              (reduced nil)))))
                    []
                    (repeatedly 3 #(get-ubyte buf)))]
    (some-> res join keyword)))

(defmethod read-flag :gamemode-name [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :gamemode-shortname [_ ^ByteBuffer buf _]
  (get-string buf))

(defmethod read-flag :voicechat [_ ^ByteBuffer buf _]
  (let [c (get-ubyte buf)]
    (case c
      0 :disabled
      1 :global
      2 :team-based
      3 :spectators-separate
      c)))

(defmethod read-flag :extended-info [_ ^ByteBuffer buf s]
  (let [rflags (get-uint buf)]
    (for-map [f parsed-extended-flags
              :when (= (flags2* f) (bit-and rflags (flags2* f)))]
      f (read-flag f buf s))))

(def- challenge-response*
  (bimap {:accepted 5660023
          :throttled 5660024
          :banned 5660025
          :segmented 5660032}))

(defn- flagset [flags]
  (reduce (fn [r f]
            (if (= (flags* f) (bit-and flags (flags* f)))
              (conj r f)
              r))
          #{}
          (->> flags* keys (filter keyword?))))

(defn- parse-complete-response [buf]
  (let [returnedtime (get-uint buf)
        _ (when-not (zero? returnedtime)
            (throw (ex-info "server echoed unexpected (nonzero) request time"
                            {:returned-time returnedtime})))
        ver (get-string buf)
        rflags (get-uint buf)]
    ;(when (and (not= query-flags rflags)
    ;           (not= short-query-flags rflags))
    ;  (trace "getting different flags" rflags (flagset rflags)
    ;         "than requested from" addr port))
    (reduce (fn [r f]
              (if (= (flags* f) (bit-and rflags (flags* f)))
                (assoc r f (read-flag f buf r))
                r))
            {:version ver}
            parsed-flags)))

(defn- parse-segmented-response [buf]
  (let [{:keys [readsize] :as header} (hash-map :number (get-ubyte buf)
                                                :total (get-ubyte buf)
                                                :offset (get-ushort buf)
                                                :readsize (get-ushort buf)
                                                :size (get-ushort buf))]
    (assoc header :data (doto>> (byte-array readsize)
                          (.get buf)))))

(defn parse-segments [size segments]
  {:pre [(= size (apply + (map count segments)))]}
  (let [buf (bytebuf size)]
    (doseq [s segments]
      (.put buf ^bytes s))
    (.rewind buf)
    (parse-complete-response buf)))

(defn parse-server-response [response]
  {:pre [(map? response)]}
  (let [{:keys [data]} response
        buf (wrapbuf (Huffman/decode data))
        code (get-uint buf)
        code* (get! challenge-response* code)]
    (when-let [e (case code*
                   :accepted nil
                   :segmented nil
                   :throttled "Server denied - throttled"
                   :banned "Server denied - IP is banned")]
      (throw (ex-info e {:code code})))
    (if (= :segmented code*)
      (parse-segmented-response buf)
      (parse-complete-response buf))))
