(ns doomlist.protocol.textcolors
  (:require [doomlist.infra.util :as u]))

(defn plain-name [zandroname]
  (letfn [(init ([res c] (let [n (long c)]
                           (cond
                             (= 0x1c n) [res code]
                             (not (< 0x1f n 0x7f)) [res init]
                             :else [(conj res c) init]))))
          (code ([res c] (if (= \[ c)
                           [res skip]
                           [res init])))
          (skip ([res c] (if (= \] c)
                           [res init]
                           [res skip])))]
    (->> zandroname
         (reduce (fn [[res f] c] (f res c)) [[] init])
         first
         u/join)))

(comment
  ((juxt plain-name #_tagged-name)
   (str (char 1) (char 28) "Lx" (char 28) "[as]d" (char 28) (char 45))))
