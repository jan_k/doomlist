(ns doomlist.sandbox
  (:import [huffman Huffman])
  (:require [doomlist.infra.util :refer :all]
            [doomlist.refresher.server :refer :all]
            [doomlist.protocol.udp :as udp]
            [doomlist.protocol.textcolors :as t]
            [doomlist.protocol.zandronum :as p]))

(defn hex [x]
  (cond
    (number? x) (format "0x%x" x)
    (string? x) (hex (.getBytes x))
    :else (join (for [b x] (format "%02x" b)))))

(defn unhex [hex]
  (->> (partition 2 hex)
       (mapv (fn [[x y]] (unchecked-byte (Integer/parseInt (str x y) 16))))
       byte-array))

(def huffpacket (unhex "025313f19224adb6f4b4743537760574f574ad367705acb276b697c448c6f14ff6e0f828a47ce517a93147fcafa5eba7a5a7f9f9afd95c24a42235fee6d1e00e0c754bbf899c5b24a4d8c7fac64848a1756eb8563c71a446fc911a1dab4b66d7ae6d85f6fd9eda74e3d896b566236d8cb4611452791ac78c351ba9b125cfa80d6f09172792c938b973199b61cc88116b9b9ab3794621b53612526b9b575b7a9a17475adbb4a7aba55586551dc9b3b6ed644f5700fee796561956b571c388e6a9bf48ad8dd438d933d9d3dc74e3c6514835e3ef6a69956155d3f2346dde3066ec6f9ee6564df36468c6dfd5d2d3d22ac3aa466d6cda76445373475a5a6558d5b4a6486d6c1c35b65563a448938d79265b5a655895d9311bd7fe6e88d468f1ffb5b4cab0aa3c6bdbe669da6a72634bab0cab422ad2d4b5918eaccd336ac4a8318d63ed645c4f4bab0cab6a28710b1e1c971f1f8b5bf0e0b8265b22737293f689081bba4568bb76c4c6081b1245688e807f6d04734d7fd9c6463a8213a1599d144ed24e5b8d68ba19a951d208ed1a4c23ace5f53b225284668f1021429eafdf3c234645d8b0e117a9b1193623344b62ae5d8315a1db88c944c84d236c686c15a11bfec9444d236cd8d83442b71193891a5b4568968444d24e2374c3d693a8db1706b74418a3c68c88d418a159122eed1a4c23ace3ff8dc01ca908eb486d46689664b0760da61136748bd0f665855b840d8922e0e78c60cedc6f04fc892398cb1069c48808cd923648da69046c89cd366d6c15a1595260da359846e8362d2e51b708dda6ad264a1cf515a1dbb4b8448922743bf29328f188a6911a374668d6aec4924423749b1697a85b846ed35613258efa8ad06d5a5ca24411ba1df949f4d574daa8c6c608cd92a2248946e8362d2e51b708dda6ad264a1cf515a1dbb4b8448922743bf2936874d30c19222115a15952489228000000000000000000000000000000000000000000000000000000000000"))

(comment
  (hex (Huffman/decode huffpacket))
  ; %! xxd -r -p | xxd
;00000000: 775d 5600 0000 0000 332e 302e 312d 7231  w]V.....3.0.1-r1
;00000010: 3931 3031 332d 3139 3338 2028 5453 5047  91013-1938 (TSPG
;00000020: 7632 3029 206f 6e20 4c69 6e75 7820 342e  v20) on Linux 4.
;00000030: 3135 2e30 2d37 342d 6765 6e65 7269 6300  15.0-74-generic.
;00000040: ff0f 1800 5b4e 595d 2044 656e 2773 2064  ....[NY] Den's d
;00000050: 656e 3a20 444d 4c20 5365 7276 6572 2023  en: DML Server #
;00000060: 3100 6874 7470 3a2f 2f77 6164 732e 7468  1.http://wads.th
;00000070: 6564 656d 6f6e 6372 7573 6865 722e 636f  edemoncrusher.co
;00000080: 6d2f 0000 4d41 5032 3600 2020 0973 6b75  m/..MAP26.  .sku
;00000090: 6c6c 7461 675f 636f 6e74 656e 742d 332e  lltag_content-3.
;000000a0: 302d 6265 7461 3031 2e70 6b33 0078 6374  0-beta01.pk3.xct
;000000b0: 6632 3031 3976 372e 706b 3300 646d 6c2d  f2019v7.pk3.dml-
;000000c0: 7769 6e74 6572 3230 3230 2d61 6464 6f6e  winter2020-addon
;000000d0: 2d76 312e 706b 3300 7a63 612d 6d75 7369  -v1.pk3.zca-musi
;000000e0: 632d 7061 636b 2d76 312e 302e 706b 3300  c-pack-v1.0.pk3.
;000000f0: 6f64 6166 6c61 6778 2e70 6b33 007a 616e  odaflagx.pk3.zan
;00000100: 6472 6f73 7072 6565 3272 6332 2e70 6b33  drospree2rc2.pk3
;00000110: 0068 7564 7469 6d65 725f 7634 2e70 6b33  .hudtimer_v4.pk3
;00000120: 0063 7466 6361 7032 642e 706b 3300 6e65  .ctfcap2d.pk3.ne
;00000130: 7774 6578 7463 6f6c 6f75 7273 5f32 3630  wtextcolours_260
;00000140: 2e70 6b33 000c 0000 444f 4f4d 2049 4900  .pk3....DOOM II.
;00000150: 444f 4f4d 322e 5741 4400 0001 0b1c 6d5b  DOOM2.WAD.....m[
;00000160: 1c66 746c 641c 6d5d 1c2d 1c76 741c 6761  .ftld.m].-.vt.ga
;00000170: 6928 7365 7829 1c2d 0002 0014 0000 0001  i(sex).-........
;00000180: 0570 6c61 7965 7200 0000 6c00 0100 ff05  .player...l.....
;00000190: 1c74 5669 6c65 1c2d 201c 1c1c 6352 6963  .tVile.- ...cRic
;000001a0: 6c6f 1c6d 6d69 6e73 6b79 1c2d 0000 004b  lo.mminsky.-...K
;000001b0: 0001 00ff 001c 5b6c 325d 4361 1c6d 7270  ......[l2]Ca.mrp
;000001c0: 1c5b 7632 5d61 1c6d 6461 1c5b 6c32 5d72  .[v2]a.mda.[l2]r
;000001d0: 701c 2d00 0000 5000 0000 0105 1c5b 7130  p.-...P......[q0
;000001e0: 5d5b 5224 445d 246f 756c 6572 1c2d 0000  ][R$D]$ouler.-..
;000001f0: 004d 0001 00ff 051c 4876 691c 4b6e 1c48  .M......Hvi.Kn.H
;00000200: 6e79 1c2d 0000 009d 0001 00ff 051c 6d5b  ny.-..........m[
;00000210: 1c66 544c 441c 6d5d 1c76 411c 6767 691c  .fTLD.m].vA.ggi.
;00000220: 7653 1c67 6b65 6c6c 1c2d 0000 006d 0000  vS.gkell.-...m..
;00000230: 0001 051c 7153 6861 7270 1c2d 0000 00b7  ....qSharp.-....
;00000240: 0001 00ff 051c 5b7a 365d 5b1c 5b7a 335d  ......[z6][.[z3]
;00000250: 5342 521c 5b7a 365d 5d1c 5b78 355d 536c  SBR.[z6]].[x5]Sl
;00000260: 6165 7264 1c2d 0001 0053 0000 0000 051c  aerd.-...S......
;00000270: 5b7a 365d 5b1c 5b7a 335d 5342 521c 5b7a  [z6][.[z3]SBR.[z
;00000280: 365d 5d1c 5b78 355d 5261 7a6f 7272 1c2d  6]].[x5]Razorr.-
;00000290: 0000 0042 0000 0000 051c 5b7a 365d 5b1c  ...B......[z6][.
;000002a0: 5b7a 335d 5342 521c 5b7a 365d 5d1c 5b78  [z3]SBR.[z6]].[x
;000002b0: 355d 5a61 6b6b 656e 1c2d 0000 0081 0000  5]Zakken.-......
;000002c0: 0000 0580 8080 8080 8080 8080 8080 8080  ................
;000002d0: 8080 8080 8080 8080 8080 8080 8080 8080  ................
;000002e0: 8080 8080 8080 8080 8080 8080 8080 8080  ................
;000002f0: 8080 8080 8080 8080 8080 8080 8080 80    ...............
  )

(def tagged-codes
  {"a" "FF91A4" "b" "D2B48C" "c" "808080" "d" "32CD32" "e" "918151" "f" "F4C430"
   "g" "E32636" "h" "0000FF" "i" "FF8C00" "j" "C0C0C0" "k" "FFD700" "l" "E34234"
   "m" "000000" "n" "4169E1" "o" "FFDEAD" "p" "465945" "q" "228b22" "r" "800000"
   "s" "704214" "t" "A020F0" "u" "404040" "v" "007F7F"
   "a0" "0fcef8" "a1" "54231d" "a2" "54ac3b" "a3" "733a41" "a4" "2c2629"
   "a5" "f4d485" "a6" "58e900" "a7" "7d81bb" "a8" "ba3575" "a9" "e02956"
   "b0" "eb51ed" "b1" "e00000" "b2" "0072ff" "b3" "009e0f" "b4" "ffea00"
   "b5" "a800ff" "b6" "ff8929" "b7" "cccccc" "b8" "763312" "b9" "ae9780"
   "c0" "b52485" "c1" "01a3e7" "c2" "5aac09" "c3" "e7c501" "c4" "a3001d"
   "c5" "c64c01" "c6" "54187d" "c7" "ce8fc6" "c8" "624b3a" "c9" "00ac98"
   "d0" "43ab00" "d1" "ad4b17" "d2" "3f2b17" "d3" "4c2d77" "d4" "c00406"
   "d5" "996035" "d6" "d8e604" "d7" "9c2656" "d8" "aba0b7" "d9" "958e88"
   "e0" "c0daf4" "e1" "9be550" "e2" "de8f00" "e3" "5e8fc5" "e4" "035ee6"
   "e5" "aa2f2e" "e6" "5b4a7c" "e7" "d66400" "e8" "e53426" "e9" "e7d606"
   "f0" "79d1e2" "f1" "c7203a" "f2" "ebff9d" "f3" "f59c8d" "f4" "886884"
   "f5" "9dc98e" "f6" "327d11" "f7" "812158" "f8" "af8504" "f9" "217281"
   "g0" "eead00" "g1" "d3d3de" "g2" "151317" "g3" "b6b4b7" "g4" "3a611b"
   "g5" "e76c1e" "g6" "c70000" "g7" "d8b55a" "g8" "473f39" "g9" "d6c086"
   "h0" "9f96b3" "h1" "d04541" "h2" "e0dcdc" "h3" "003ee2" "h4" "74a3cd"
   "h5" "0bac0e" "h6" "7e5fb4" "h7" "8e4500" "h8" "acb52b" "h9" "2d7043"
   "i0" "d4d57b" "i1" "eab141" "i2" "6d2e1a" "i3" "fab3f4" "i4" "64ab61"
   "i5" "b5ca44" "i6" "89715d" "i7" "8b0201" "i8" "17a98c" "i9" "6f189b"

   "j0" "397699"
   "j1" "1f71e1"
   "j2" "feb5e9"
   "j3" "c86700"
   "j4" "a1805d"
   "j5" "8aff3a"
   "j6" "7d5733"
   "j7" "b39e5d"
   "j8" "950bc1"
   "j9" "897581"

   "k0" "0fcef8" "k1" "54231d" "k2" "54kc3b" "k3" "733k41" "k4" "2c2629"
   "k5" "f4d485" "k6" "58e900" "k7" "7d81bb" "k8" "bk3575" "k9" "e02956"
   "l0" "0fcef8" "l1" "54231d" "l2" "54lc3b" "l3" "733l41" "l4" "2c2629"
   "l5" "f4d485" "l6" "58e900" "l7" "7d81bb" "l8" "bl3575" "l9" "e02956"
   "m0" "0fcef8" "m1" "54231d" "m2" "54mc3b" "m3" "733m41" "m4" "2c2629"
   "m5" "f4d485" "m6" "58e900" "m7" "7d81bb" "m8" "bm3575" "m9" "e02956"
   "n0" "0fcef8" "n1" "54231d" "n2" "54nc3b" "n3" "733n41" "n4" "2c2629"
   "n5" "f4d485" "n6" "58e900" "n7" "7d81bb" "n8" "bn3575" "n9" "e02956"
   "o0" "0fcef8" "o1" "54231d" "o2" "54oc3b" "o3" "733o41" "o4" "2c2629"
   "o5" "f4d485" "o6" "58e900" "o7" "7d81bb" "o8" "bo3575" "o9" "e02956"
   "p0" "0fcef8" "p1" "54231d" "p2" "54pc3b" "p3" "733p41" "p4" "2c2629"
   "p5" "f4d485" "p6" "58e900" "p7" "7d81bb" "p8" "bp3575" "p9" "e02956"
   "q0" "0fcef8" "q1" "54231d" "q2" "54qc3b" "q3" "733q41" "q4" "2c2629"
   "q5" "f4d485" "q6" "58e900" "q7" "7d81bb" "q8" "bq3575" "q9" "e02956"
   "r0" "0fcef8" "r1" "54231d" "r2" "54rc3b" "r3" "733r41" "r4" "2c2629"
   "r5" "f4d485" "r6" "58e900" "r7" "7d81bb" "r8" "br3575" "r9" "e02956"
   "s0" "0fcef8" "s1" "54231d" "s2" "54sc3b" "s3" "733s41" "s4" "2c2629"
   "s5" "f4d485" "s6" "58e900" "s7" "7d81bb" "s8" "bs3575" "s9" "e02956"
   "t0" "0fcef8" "t1" "54231d" "t2" "54tc3b" "t3" "733t41" "t4" "2c2629"
   "t5" "f4d485" "t6" "58e900" "t7" "7d81bb" "t8" "bt3575" "t9" "e02956"
   "u0" "0fcef8" "u1" "54231d" "u2" "54uc3b" "u3" "733u41" "u4" "2c2629"
   "u5" "f4d485" "u6" "58e900" "u7" "7d81bb" "u8" "bu3575" "u9" "e02956"
   "v0" "0fcef8" "v1" "54231d" "v2" "54vc3b" "v3" "733v41" "v4" "2c2629"
   "v5" "f4d485" "v6" "58e900" "v7" "7d81bb" "v8" "bv3575" "v9" "e02956"
   "w0" "0fcef8" "w1" "54231d" "w2" "54wc3b" "w3" "733w41" "w4" "2c2629"
   "w5" "f4d485" "w6" "58e900" "w7" "7d81bb" "w8" "bw3575" "w9" "e02956"
   "x0" "0fcef8" "x1" "54231d" "x2" "54xc3b" "x3" "733x41" "x4" "2c2629"
   "x5" "f4d485" "x6" "58e900" "x7" "7d81bb" "x8" "bx3575" "x9" "e02956"
   "y0" "0fcef8" "y1" "54231d" "y2" "54yc3b" "y3" "733y41" "y4" "2c2629"
   "y5" "f4d485" "y6" "58e900" "y7" "7d81bb" "y8" "by3575" "y9" "e02956"
   "z0" "0fcef8" "z1" "54231d" "z2" "54zc3b" "z3" "733z41" "z4" "2c2629"
   "z5" "f4d485" "z6" "58e900" "z7" "7d81bb" "z8" "bz3575" "z9" "e02956"})

(comment
  (->> (current-serverlist)
       (filterv (fn [[id s]]
                  (and (< 3 (count (:pwads s)))
                       (not-any? #(starts-with? (lower-case %) "oblige")
                                 (:pwads s))
                       (re-seq #"^Doom(.II)?$" (:mod s "")))))
       vals
       (mapv #(select-keys % [:name :iwad :pwads]))
       inspect))

(comment
  (def a (atom 1))
  (swap! a (fn [x] (throw (ex-info "a" {})))))

(comment
  (hex 1834078786))

(comment
  (join (map #(char (Byte/toUnsignedLong (byte %)))
             [;-127, 126, 18, 21, 45, 91, 91, 58, 115, 101, 114, 118, 101, 114, 47, 117, 112, 100, 97, 116, 101, 45, 116, 115, 32, 91, 34, 49, 53, 55, 46, 50, 52, 53, 46, 49, 50, 53, 46, 50, 52, 48, 58, 49, 48, 54, 54, 54, 34, 32, 49, 53, 56, 48, 54, 49, 52, 52, 50, 57, 50
              ])))

(comment
  (def sente @#'doomlist.server.websocket/sente)
  (:any @(:connected-uids @sente))
  (doseq [x (range 300)]
    (let [send (:send-fn @sente)
          uid (first (:any @(:connected-uids @sente)))]
      (send uid [::spam (range 300)] {:flush? true}))))
