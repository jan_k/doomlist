(ns doomlist.figwheel
  (:require [figwheel.main.api :as fw]
            [cider.piggieback]
            [doomlist.infra.util :refer :all]))

(def- figwheel-config
  {:id "client"
   :config {:css-dirs ["resources/public/css"]
            :ring-server-options {:port 9501}
            :watch-dirs ["src"]
            :wait-time-ms 150
            :load-warninged-code false
            :open-url false}
   :options {:main 'doomlist.ui.main
             :asset-path "js/compiled/client"
             :output-to "resources/public/js/compiled/client.js"
             :output-dir "resources/public/js/compiled/client"
             :source-map-timestamp true
             :preloads ['devtools.preload]}})

(defn figwheel []
  (fw/start {:mode :serve :css-dirs ["resources/public/css"]} figwheel-config))

(defn stop-figwheel []
  (fw/stop-all))
