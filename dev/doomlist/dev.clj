(ns doomlist.dev
  (:require [clojure.repl :refer :all :exclude [pst]]
            [clojure.tools.namespace.repl :refer [refresh refresh-all]]
            [clojure.pprint]
            [mount.core]
            [doomlist.figwheel :refer :all]
            [doomlist.infra.main :refer :all]
            [doomlist.infra.util :refer :all]))

(intern 'clojure.core 'pprint clojure.pprint/pprint)
(intern 'clojure.core 'pst clojure.repl/pst)

(clojure.tools.namespace.repl/set-refresh-dirs "src")

;(set! *warn-on-reflection* true)
(set! clojure.core/*print-length* 150)
(set! clojure.core/*print-level* 10)

(defn stop []
  (mount.core/stop))

(defn reset []
  (stop)
  (refresh-all :after `start))

(defn init []
  (if (System/getenv "AUTORUN")
    (refresh :after `start)))

(defstate* figwheel-dev
  :start (figwheel)
  :stop (stop-figwheel))

(comment
  (inspect (sort-by (comp :order val) @@#'mount.core/meta-state)))
