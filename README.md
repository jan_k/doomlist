# doomlist.net

Website with a live list of Zandronum servers

https://doomlist.net/

## Q-Zandronum instance:

https://q.doomlist.net/

## JSON data endpoints:

https://doomlist.net/api - only populated servers

https://doomlist.net/api/full - all servers

https://q.doomlist.net/api - Q-Zandronum servers

The API allows you to get machine-readable information about all servers in a single HTTP request, without having to deal with Zandronum's binary UDP protocols and without having to query each individual server.  With this you can build your own server browser on top, monitor your servers etc.  Feel free to use it for any non-malicious purpose.

If you want the freshest data I recommend polling every 30 seconds.  More frequent polling than that will not be useful as you will likely only get the same cached result.

Do not expect 100% availability, the server will sometimes go down while I deploy updates, but usually only for a minute or two.

Here are some shell-based examples that use `curl` to query the API and extract info from the JSON using `jq`:

```
# extract address, name and WADs of each server:
curl https://doomlist.net/api | jq '.[] | {addr, port, name, pwads}'

# see what data keys are available, by looking at the first server:
curl https://doomlist.net/api | jq 'map(.) | .[0] | keys[]'
```

## Documentation?

If you're interested in running your own instance, or would like to see some other documentation, please let me know (eg. create a new issue here on Gitlab). I'll be happy to write some docs if there is actual interest.
