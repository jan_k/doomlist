package huffman;

public class Huffman {
    static {
        String path = System.getProperty("huffman.jni.path");
        if (path == null) {
            throw new IllegalStateException("Missing JNI library path, run with -Dhuffman.jni.path=/path/to/huffman.so");
        }
        System.load(path);
        HUFFMAN_Construct();
    }

    native static void HUFFMAN_Construct();

    native static byte[] HUFFMAN_Decode(byte[] in);

    native static byte[] HUFFMAN_Encode(byte[] in);

    public static byte[] decode(byte[] in) {
        byte[] res = HUFFMAN_Decode(in);
        if (res == in) {
            throw new IllegalStateException("Failed to decode packet");
        }
        return res;
    }

    public static byte[] encode(byte[] in) {
        byte[] res = HUFFMAN_Encode(in);
        if (res == in) {
            throw new IllegalStateException("Failed to encode packet");
        }
        return res;
    }
}
