#include "huffman_Huffman.h"
#include "huffman/huffman.h"

// g++ -fPIC -W -shared -I/usr/lib/jvm/java-11-openjdk-amd64/include -I/usr/lib/jvm/java-11-openjdk-amd64/include/linux -o huffman.so huffman/*.cpp huffman_Huffman.cpp

    JNIEXPORT void JNICALL Java_huffman_Huffman_HUFFMAN_1Construct
(JNIEnv* env, jclass c)
{
    HUFFMAN_Construct();
}

/*
 * Class:     huffman_Huffman
 * Method:    decode
 * Signature: ([B)[B
 */
    JNIEXPORT jbyteArray JNICALL Java_huffman_Huffman_HUFFMAN_1Decode
(JNIEnv* env, jclass c, jbyteArray in) 
{
    const int BUFFER_SIZE = 8000;
    jsize size = env->GetArrayLength(in);
    jsize limit = BUFFER_SIZE + size;
    jsize decoded_size = limit;
    signed char* res = new signed char[limit];

    jbyte* body = env->GetByteArrayElements(in, 0);
    HUFFMAN_Decode(reinterpret_cast<unsigned char*> (body),
            reinterpret_cast<unsigned char*> (res), size,
            &decoded_size);
    env->ReleaseByteArrayElements(in, body, 0);

    if (!decoded_size || decoded_size >= limit)
    {
        delete[] res;
        return in;
    }

    jbyteArray decoded = env->NewByteArray(decoded_size);
    env->SetByteArrayRegion(decoded, 0, decoded_size, res);
    delete[] res;
    return decoded;
}

/*
 * Class:     huffman_Huffman
 * Method:    encode
 * Signature: ([B)[B
 */
    JNIEXPORT jbyteArray JNICALL Java_huffman_Huffman_HUFFMAN_1Encode
(JNIEnv* env, jclass c, jbyteArray in)
{
    jsize size = env->GetArrayLength(in);
    jsize limit = 2*size;
    jsize encoded_size = limit;
    signed char* res = new signed char[limit];

    jbyte* body = env->GetByteArrayElements(in, 0);
    HUFFMAN_Encode(reinterpret_cast<unsigned char*> (body),
            reinterpret_cast<unsigned char*> (res),
            size, &encoded_size);
    env->ReleaseByteArrayElements(in, body, 0);

    if (!encoded_size || encoded_size >= limit)
    {
        delete[] res;
        return in;
    }

    jbyteArray encoded = env->NewByteArray(encoded_size);
    env->SetByteArrayRegion(encoded, 0, encoded_size, res);
    delete[] res;
    return encoded;
}
