# How to make Address links on doomlist.net work

To be able to join servers directly by clicking the links in the Address column on [https://doomlist.net](https://doomlist.net), you need to configure your OS to associate the `zan://` URL scheme with a launcher.

When Zandronum version 3.2 will be released, the Windows installer will create this association automatically and no setup will be needed by the user. For now you can follow this guide to do the same manually.

This guide will be using [Doomseeker](https://doomseeker.drdteam.org/) as the launcher and assumes you already have it installed.

## Guide for Linux:

Use your favorite text editor to create a file on the following path: 

`$HOME/.local/share/applications/doomseeker-link-handler.desktop`

The contents of the file should be as follows (replace `PATH_TO_DOOMSEEKER` with the correct path to the doomseeker executable on your system):

----

    [Desktop Entry]
    Type=Application
    Name=Doomseeker link handler
    Exec=/PATH_TO_DOOMSEEKER/doomseeker --connect %u
    StartupNotify=false
    MimeType=x-scheme-handler/zan;

----

Finally run the following command:

`xdg-mime default doomseeker-link-handler.desktop x-scheme-handler/zan`

Now when you click a zan:// link, Doomseeker should connect you to the server.

## Guide for Windows:

It is neccessary to edit the Windows registry to make a new URL scheme association.  You can use the linked web tool or follow the manual instructions below.

### Using Sean's web tool

[https://firestick.games/zds](https://firestick.games/zds)

Just run the generated .reg file as administrator and you should be set.

### Manual instructions

The following subtree has to be created in the registry:

----

    HKEY_CLASSES_ROOT
         zan
             (Default) = "URL:zandronum"
             URL Protocol = ""
             shell
                 open
                     command
                         (Default) = "PATH_TO_DOOMSEEKER\doomseeker.exe --connect %1"

----

To create this subtree automatically you can use [this .reg file](/zan_link_association.reg)

The last step is to replace the placeholder path with the actual path to your `doomseeker.exe`. Start the Windows registry editor (`regedit.exe`) as administrator and navigate to the following path in the registry:

`Computer\HKEY_CLASSES_ROOT\zan\shell\open\command`

Double click the `(Default)` entry to edit the value. Replace `PATH_TO_DOOMSEEKER` with the path to the doomseeker executable on your system.

The result should look like this (except the path will be different):

![regedit](/regedit.png)

Once the path to doomseeker.exe in the registry is correctly set the `zan://` links should start working immediately.
